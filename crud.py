from sqlalchemy.orm import Session
from models import DBProduct
from schemas import ProductCreate

def create_product(db: Session, product: ProductCreate):
    db_product = DBProduct(**product.dict())
    db.add(db_product)
    db.commit()
    db.refresh(db_product)
    return db_product

def get_product(db: Session, product_id: int):
    return db.query(DBProduct).filter(DBProduct.id == product_id).first()

def list_products(db: Session, skip: int = 0, limit: int = 10):
    return db.query(DBProduct).offset(skip).limit(limit).all()

def update_product(db: Session, product_id: int, product: ProductCreate):
    db_product = db.query(DBProduct).filter(DBProduct.id == product_id).first()
    if db_product:
        for key, value in product.dict().items():
            setattr(db_product, key, value)
        db.commit()
        db.refresh(db_product)
        return db_product

def delete_product(db: Session, product_id: int):
    db_product = db.query(DBProduct).filter(DBProduct.id == product_id).first()
    if db_product:
        db.delete(db_product)
        db.commit()
        return db_product
