from fastapi.testclient import TestClient
from main import app
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from database import Base, engine, SessionLocal
from models import Product as DBProduct
from schemas import ProductCreate, Product as ProductResponse
import json
import pytest

# Initialize a test database
@pytest.fixture(scope="module")
def test_db():
    test_engine = create_engine("postgresql://anasr7@localhost/edgeworks_db")
    TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=test_engine)
    Base.metadata.create_all(bind=test_engine)
    try:
        yield TestingSessionLocal
    finally:
        test_engine.dispose()

# Create a FastAPI test client
client = TestClient(app)

# Example data for testing
test_product_data = {
    "name": "Test Product",
    "description": "This is a test product description.",
    "image": "https://example.com/test_product_image.jpg",
    "price": 19.99,
    "aggregateRating": json.dumps({
        "ratingValue": 4.0,
        "reviewCount": 5
    }),
    "review": json.dumps([
        {
            "author": "Test Author",
            "datePublished": "2023-10-18",
            "reviewBody": "This is a test review.",
            "name": "Test Review",
            "reviewRating": json.dumps({
                "ratingValue": 4.0,
                "reviewCount": 1
            })
        }
    ])
}

# Test POST request to create a product
def test_create_product():
    response = client.post("/products/", json=test_product_data)
    assert response.status_code == 200
    product = response.json()
    assert "id" in product
    assert product == test_product_data

# Test GET request to read a product
def test_read_product():
    response = client.post("/products/", json=test_product_data)
    assert response.status_code == 200
    product = response.json()

    response = client.get(f"/products/{product['id']}")
    assert response.status_code == 200
    assert response.json() == product

# Test PUT request to update a product
def test_update_product():
    response = client.post("/products/", json=test_product_data)
    assert response.status_code == 200
    product = response.json()

    update_data = {
        "name": "Updated Test Product",
        "description": "Updated description.",
        "price": 29.99
    }

    response = client.put(f"/products/{product['id']}", json=update_data)
    assert response.status_code == 200
    updated_product = response.json()
    assert updated_product["name"] == update_data["name"]
    assert updated_product["description"] == update_data["description"]
    assert updated_product["price"] == update_data["price"]

# Test DELETE request to delete a product
def test_delete_product():
    response = client.post("/products/", json=test_product_data)
    assert response.status_code == 200
    product = response.json()

    response = client.delete(f"/products/{product['id']}")
    assert response.status_code == 200
    deleted_product = response.json()
    assert deleted_product == product
