from fastapi import FastAPI, Depends, HTTPException
from sqlalchemy.orm import Session
from database import engine, SessionLocal
from crud import create_product, get_product, list_products, update_product, delete_product
from schemas import ProductCreate, ProductResponse

app = FastAPI()

# Dependency to get the database session
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

# Create a new product
@app.post("/products/", response_model=ProductResponse, summary="Create a new product")
def create_new_product(product: ProductCreate, db: Session = Depends(get_db)):
    """
    Create a new product.

    - **product**: The details of the product to create.
    - **db**: The database session.

    Returns the created product.
    """
    return create_product(db, product)

# Get a product by ID
@app.get("/products/{product_id}", response_model=ProductResponse, summary="Get a product by ID")
def read_product(product_id: int, db: Session = Depends(get_db)):
    """
    Get a product by its ID.

    - **product_id**: The ID of the product to retrieve.
    - **db**: The database session.

    Returns the product details.
    """
    product = get_product(db, product_id)
    if product is None:
        raise HTTPException(status_code=404, detail="Product not found")
    return product

# List all products
@app.get("/products/", response_model=list[ProductResponse], summary="List all products")
def read_products(skip: int = 0, limit: int = 10, db: Session = Depends(get_db)):
    """
    List all products.

    - **skip**: The number of products to skip (pagination).
    - **limit**: The maximum number of products to return (pagination).
    - **db**: The database session.

    Returns a list of products.
    """
    return list_products(db, skip, limit)

# Update a product by ID
@app.put("/products/{product_id}", response_model=ProductResponse, summary="Update a product by ID")
def update_existing_product(product_id: int, product: ProductCreate, db: Session = Depends(get_db)):
    """
    Update a product by its ID.

    - **product_id**: The ID of the product to update.
    - **product**: The updated product details.
    - **db**: The database session.

    Returns the updated product.
    """
    updated_product = update_product(db, product_id, product)
    if updated_product is None:
        raise HTTPException(status_code=404, detail="Product not found")
    return updated_product

# Delete a product by ID
@app.delete("/products/{product_id}", response_model=ProductResponse, summary="Delete a product by ID")
def delete_existing_product(product_id: int, db: Session = Depends(get_db)):
    """
    Delete a product by its ID.

    - **product_id**: The ID of the product to delete.
    - **db**: The database session.

    Returns the deleted product.
    """
    deleted_product = delete_product(db, product_id)
    if deleted_product is None:
        raise HTTPException(status_code=404, detail="Product not found")
    return deleted_product

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)
