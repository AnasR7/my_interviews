from pydantic import BaseModel, PositiveFloat, PositiveInt

class ProductBase(BaseModel):
    name: str
    description: str
    image: str
    price: PositiveFloat

class AggregateRating(BaseModel):
    ratingValue: PositiveFloat
    reviewCount: PositiveInt

class Review(BaseModel):
    author: str
    datePublished: str
    reviewBody: str
    name: str
    reviewRating: AggregateRating

class ProductCreate(ProductBase):
    aggregateRating: AggregateRating
    review: list[Review]

class ProductResponse(ProductBase):
    id: int
    aggregateRating: AggregateRating
    review: list[Review]
